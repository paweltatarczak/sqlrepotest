import os

print ("Username: ", os.environ['DB_CREDS_USR'] )
print ("Password: ", os.environ['DB_CREDS_PSW'] )

for char in os.environ['DB_CREDS_USR']:
    print(char,end=" ")

print("\n")

for char in os.environ['DB_CREDS_PSW']:
    print(char,end=" ")
